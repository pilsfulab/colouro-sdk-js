/**
 * Colouro SDK for Javascirpt.
 * @module colouro
 */

"use strict";

// import websocket API
var wsWebSocket = (!process.browser) ? require('ws') : WebSocket;

/**
 * Creates new instance of Colouro Client connected to `opts.host`.
 * This instance will periodically reconnect if connection is closed or initial connection fails.
 * @param {Object} opts - Options object containing at least `host` key.
 * @param {String} opts.host - Hostname and port of Colouro server e.g. '192.168.0.42:8080'
 * @param {Client~colorCalback} opts.colorHandlerFunc - Color update handler function accepting first argument as list of colors,
 * and second as list of color semantics.
 * @param {function} opts.verbose - Verbose message handler.
 * @param {number} opts.handshakeTimeout - timeout of handshake (connection phase) in ms (works only in Node.js environment).
 * @constructor
 * @example
 *  var client = new Client({host:'192.168.0.42:8080'})
 *      .on('status', function (isConnected){
 *          console.log('Client is ' + (isConnected ? 'connected' : 'disconnected'));
 *      })
 *      .on('colors', function (colors, semantics) {
 *          for (var i = 0; i < 5; i++) {
 *              console.log('Semantic: ' + semantics[i] + '  color: ' + colors[i]);
 *          }
 *      });
 */
function Client(opts) {
    this.closing = true;
    this.verbose = opts.verbose || false;
    this.ws = null;
    this.timer = null;
    this.colorHandlerFunc = opts.colorHandlerFunc || function(){};
    this.events = {status:[], colors:[]};
    this.handshakeTimeout = opts.handshakeTimeout || 1000;
    if (process.browser) {
        this.setHost(opts.host || window.location.host);
    } else {
        this.setHost(opts.host);
    }
}

/**
 * Sets host to connect to.
 * @param host Host name and port in format 'host:port', e.g. '192.168.0.42:8080'.
 */
Client.prototype.setHost = function(host) {
    var url = 'ws://' + host + '/colors';
    if (url !== this.url) {
        this.url = url;
        this.close();
        this.init();
    }
};

/**
 * Open connection.
 * Note that connection is already opened after Client instance is created.
 */
Client.prototype.init = function() {
    this.closing = false;
    this.ws
        = (process.browser)
        ? new WebSocket(this.url)
        : new wsWebSocket(this.url, null, {handshakeTimeout: this.handshakeTimeout})
    ;

    var verbose = this.verbose;
    var recon = this.reconnect.bind(this);
    var msgRecv = this.onMessageReceived.bind(this);
    var invokeFn = this.invokeStatusChanged.bind(this);
    var self = this;
    this.ws.onopen = function(evnt) {
        invokeFn(true);
    };
    this.ws.onmessage = function(evnt) {
        var data = evnt.data;
        msgRecv(data);
    };
    this.ws.onerror = function (evnt) {
        if (typeof verbose === 'function') {
            verbose(evnt);
        }
    };

    this.ws.onclose = function(evnt) {
        invokeFn(false);
        if (!self.closing) {
            setTimeout(recon, 1000);
        }
    };
};

/**
 * Register callback to listen to given event eventName.
 * @param {String} eventName - Name of event to register callback to listen.
 *
 * Valid are:
 * <ul>
 * <li>'status' - Connection state change event,</li>
 * <li>'colors' - Color update received event.</li>
 * </ul>
 * @param {(Client~statusCallback|Client~colorCalback)} callback
 * @returns {Client} this instance.
 * @example
 *  var client = new Client({host:'192.168.0.42:8080'})
 *      .on('status', function (isConnected){
 *          console.log('Client is ' + (isConnected ? 'connected' : 'disconnected'));
 *      })
 *      .on('colors', function (colors, semantics) {
 *          for (var i = 0; i < 5; i++) {
 *              console.log('Semantic: ' + semantics[i] + '  color: ' + colors[i]);
 *          }
 *      });
 */
Client.prototype.on = function(eventName, callback) {
    var cbs = this.events[eventName];
    if (cbs) {
        cbs.push(callback);
    }
    return this;
};

Client.prototype.invokeStatusChanged = function(status) {
    var i, cbs = this.events.status;
    for (i = 0; i < cbs.length; i++) {
        cbs[i](status);
    }
};

Client.prototype.invokeColorsChanged = function(colors, semantics) {
    var cbs = this.events.colors;
    for (var i = 0; i < cbs.length; i++) {
        cbs[i](colors, semantics);
    }
};

Client.prototype.onMessageReceived = function(data) {
    var colorData = data.split(",");
    var colors = [];
    var semantics = [];
    for (var i = 0; i < colorData.length; i++) {
        var tokens = colorData[i].split("|");
        colors.push(tokens[0]);
        semantics.push(parseInt(tokens[1]));
    }
    this.colorHandlerFunc(colors, semantics);
    this.invokeColorsChanged(colors, semantics);
};

/**
 * Reconnect to host.
 */
Client.prototype.reconnect = function() {
    //this.ws.close();
    this.ws = null;
    try {
        this.init();
    } catch (e) {
        if (typeof this.verbose === 'function') {
            this.verbose(e);
        }
    }
};

/**
 * Close connection.
 */
Client.prototype.close = function () {
    if (this.ws === null || this.closing) {
        return;
    }
    this.closing = true;
    this.ws.close();
};

//module.exports = Client;
export default Client;

/**
 * This callback is called when new color schema is published.
 * @callback Client~colorCalback
 * @param {string[]} colors Array of colors (length=5) as strings in '#ffffff' format.
 * @param {number[]} semantics Array of semantics (meaning of colors, length=5) as intgers.
 * Following semantics are used:
 * <ul>
 *     <li>0---primary foreground</li>
 *     <li>1---primary background</li>
 *     <li>2---secondary foreground</li>
 *     <li>3---secondary background</li>
 *     <li>4---mid-ground</li>
 */

/**
 * This callback is called when client changes connection status.
 * @callback Client~statusCallback
 * @param {boolean} isConnected
 */
