"use strict";

/**
 * @typedef {Object} ColouroSyncHandle
 * @property {function} close Quits the colouroSync.
 * @property {function} open Starts the colouroSync again (after being closed).
 */

/**
 * Runs the colouro sync in background. It connects to given Colouro host,
 * and saves the colors to file using given template.
 * @param {Object} opts Options to configure the sync.
 * @param {String} opts.host Address and port to colouro host, if stated overrides the config value  (default: '192.168.0.42:8080')
 * @param {String} opts.configFile Path to colouro.json config file. (default: './colouro.json')
 * @param {boolean} opts.logging Enable console output useful for CLI tool (default: false)
 * @param {String} opts.dest Path to destination file (no default, requires to be stated in opts or config file).
 * @param {String} opts.templateFile Path to template file. (default: config value or simple built-in template will be used)
 * @param {boolean} opts.verbose Enable verbose console output. (default: false)
 * @return {ColouroSyncHandle} handle for closing the colouroSync.
 */
 function colouroSync (opts) {
    opts = opts || {};

    const util = require('util'),
        fs = require('fs'),
        Client = require('./colouro-sdk.js'),
        semanticVarName = ['fg-maj', 'bg-maj', 'fg-min', 'bg-min', 'mg'];

    function Logger() {
        this._verbose = false;
        this._silent = true;
        this._lastStatus = null;
        this._spinner = 0;
        this._spinnerElements = [
            '         ',
            '\u25dc        ',
            '\u25dc\u25dd       ',
            '\u25dc\u25dd\u25df      ',
            '\u25dc\u25dd\u25df\u25de     ',
            '\u25dc\u25dd\u25df\u25de\u25dc    ',
            ' \u25dd\u25df\u25de\u25dc\u25dd   ',
            '  \u25df\u25de\u25dc\u25dd\u25df  ',
            '   \u25de\u25dc\u25dd\u25df\u25de ',
            '    \u25dc\u25dd\u25df\u25de\u25dc',
            '     \u25dd\u25df\u25de\u25dc',
            '      \u25df\u25de\u25dc',
            '       \u25de\u25dc',
            '        \u25dc',
        ];

    }

    Logger.prototype.setVerbose = function (verbose) {
        this._verbose = verbose;
    };

    Logger.prototype.setSilent = function (silent) {
        this._silent = silent;
    };

    Logger.prototype.writeStatus = function (msg) {
        if (this._silent)
            return;
        if (process.stdout.clearLine !== undefined) {
            process.stdout.clearLine();
        }
        process.stdout.write('\r' + msg);
        this._lastStatus = msg;
    };

    Logger.prototype.writeStatusSpinner = function (msg) {
        if (this._silent)
            return;
        this.writeStatus(msg + ' ' + this._spinnerElements[this._spinner++]);
        if (this._spinner >= this._spinnerElements.length) {
            this._spinner = 0;
        }
    };

    Logger.prototype.log = function(msg) {
        if (this._silent)
            return;
        if (this._lastStatus != null) {
            if (process.stdout.clearLine !== undefined) {
                process.stdout.clearLine();
            }
            process.stdout.write('\r');
        }
        if (typeof msg === 'object') {
            msg = util.inspect(msg, {depth:5});
        }
        process.stdout.write(msg + '\n');
        if (this._lastStatus != null) {
            this.writeStatus(this._lastStatus);
        }
    };

    Logger.prototype.verbose = function(msg) {
        if (!this._verbose) {
            return;
        }

        this.log(msg);
    };

    Logger.prototype.error = function(msg) {
        //if (this._silent)
        //    return;
        this.log(msg);
    };

    Logger.prototype.warning = function(msg) {
        //if (this._silent)
        //    return;
        this.log(msg);
    };

    function updateObject(destObj, sourceObj) {
        for (var key in sourceObj) {
            if (sourceObj.hasOwnProperty(key)) {
                destObj[key] = sourceObj[key];
            }
        }
    }

    function extendFileName(fn, ext) {
        var dotIdx = fn.lastIndexOf('.');
        if (dotIdx < 0) {
            return fn + ext;
        }

        var baseName = fn.substring(0, dotIdx),
            suffix = fn.substring(dotIdx);
        return baseName + ext + suffix;
    }

    function getColorYIQ(h) {
        var r = parseInt(h.substr(1,2), 16),
            g = parseInt(h.substr(3,2), 16),
            b = parseInt(h.substr(5,2), 16);
        return (r * 299 + g * 587 + b * 114) / 1000.0;
    }

    function getBlackWhiteColorFor(h) {
        return getColorYIQ(h) > 127 ? '#000000' : '#ffffff';
    }

    var logger = new Logger(),
        client = null,
        verbose = false,
        host = opts.host || '192.168.0.42:8080',
        configFile = opts.configFile || './colouro.json',
        localConfigFile = extendFileName(configFile, '.local'),
        config = fs.existsSync(configFile) ? JSON.parse(fs.readFileSync(configFile)) : {},
        fn = 'colors.scss',
        template =
            '$primary-foreground:    {{0}};\n' +
            '$primary-background:    {{1}};\n' +
            '$secondary-foreground:  {{2}};\n' +
            '$secondary-background:  {{3}};\n' +
            '$middleground:          {{4}};\n';

    logger.setSilent(!opts.logging);

    if (fs.existsSync(localConfigFile)) {
        var localConfig = JSON.parse(fs.readFileSync(localConfigFile));
        for (var key in localConfig) {
            if (localConfig.hasOwnProperty(key)) {
                config[key] = localConfig[key];
            }
        }
    }
    if ('verbose' in config) {
        verbose = config['verbose'];
    }
    if ('host' in config && !opts.host) {
        host = config['host'];
    }

    var templateFile = opts.templateFile || config.templateFile || config['template-file'];

    if (templateFile) {
        logger.verbose('Reading template file ' + templateFile);
        template = fs.readFileSync(templateFile, 'utf8');
        if (verbose) {
            for (var i = 0; i < 5; i++) {
                var s = '{{' + i + '}}';
                if (template.indexOf(s) < 0) {
                    logger.warning('WARNING: Missing ' + s + ' in template file ' + templateFile);
                }
            }
        }
    }
    if ('dest' in config) {
        fn = config['dest'];
    }

    fn = opts.dest || fn;

    logger.setVerbose(verbose);

    function getClient() {
        logger.verbose('Connecting to ' + host);
        logger.writeStatusSpinner('Connecting');
        return new Client({
            host: host,
            colorHandlerFunc: function (colors, semantics) {
                var s = template;
                for (var i = 0; i < 5; i++) {
                    var col = colors[i],
                        sem = semantics[i],
                        semName = semanticVarName[sem];
                    // expand {{0}}
                    s = s.replace(new RegExp('\\{\\{' + sem + '\\}\\}', 'g'), col);
                    // expand {{fg-maj}}
                    s = s.replace('{{' + semName + '}}', col);

                    // expand {{fg-maj-bw}}
                    s = s.replace('{{' + semName + '-bw}}', getBlackWhiteColorFor(col));
                }
                if (!fs.exists(fn)) {
                    fs.writeFileSync(fn, s);
                }
                logger.verbose('Data received');
                logger.writeStatusSpinner('Connected');

            },
            verbose: verbose ? function (msg){ logger.verbose(msg); } : null
        }).on('status', function (connected) {
            if (connected) {
                logger.writeStatus('Connected');
            } else {
                logger.writeStatusSpinner('Disconnected');
            }
        });
    }

    var quitting = false;

    function connect() {
        if (quitting) {
            return;
        }
        try {
            client = getClient();
        } catch (e) {
            logger.verbose(e);
            setTimeout(connect, 1000);
        }
    }

    function open() {
        quitting = false;
        connect();
    }

    function close() {
        quitting = true;
        if (client != null) {
            client.close();
            client = null;
        }
    }

    connect();

    return {open: open, close: close, _config: config, _opts: opts, _host: host};
}

module.exports = colouroSync;
