var assert = require('assert'),
    fs = require('fs'),
    WebSocket = require('ws'),
    Client = require('../lib/colouro-sdk'),
    colouroSync = require('../lib/sync'),
    server = null,
    client = null;

function setupServer() {
    server = new WebSocket.Server({port:8088});
    server.on('connection', function (ws) {
        //         mg      bg-min    fg-min    bg-maj    fg-maj
        ws.send('#00007f|4,#ffff11|3,#ffff22|2,#ffff33|1,#ffff44|0');
    });
}

function shutdownSever() {
    if (server != null) {
        server.close();
        server = null;
    }
}

describe('Client', function () {
    before(setupServer);
    after(shutdownSever);
    afterEach(function () {
        if (client != null) {
            client.close();
            client = null;
        }
    });
    describe('handler', function () {
       it('should receive colors', function (done) {
           client = new Client({
               host:'localhost:8088',
               colorHandlerFunc: function (colors, semantics) {
                   assert.equal(colors.length, 5);
                   assert.equal(semantics.length, 5);
                   for (var i = 0; i < 5; i++) {
                       assert.equal(colors[i], semantics[i] === 4 ? '#00007f' : ('#ffff' + i + i));
                       assert.equal(semantics[i], 4 - i);
                   }
                   // client.close();
                   // client = null;
                   done();
               }
           });
           this.timeout(1000);
       });

       it('should receive disconnect notification', function(done){
           var expectedConn = true, invokeCount = 0;
           this.timeout(1000);
           client = new Client({
               host: 'localhost:8088',
               colorHandlerFunc: function (){
                   expectedConn = false;
                   server.close();
                   //server = null;
               }
           }).on('status', function (conn){
               invokeCount++;
               assert.equal(conn, expectedConn);
               if (expectedConn === false) {
                   client.close();
                   client = null;
                   done();
               }
           });
       });
    });
});

describe('colouroSync', function() {
    before(setupServer);
    after(shutdownSever);

    it('should receive colors', function(done){
        const host = 'localhost:8088',
            destFile = __dirname + '/test.scss.test';

        // first remove the test file
        if (fs.existsSync(destFile)) {
            fs.unlinkSync(destFile);
        }

        var
            handle = colouroSync({
                host: host,
                configFile: __dirname + '/colouro.json',
                templateFile: __dirname + '/test.scss.in',
                dest: destFile
            });

        this.timeout(1000);
        setTimeout(function(){
            assert.equal(host, handle._host);
            assert(handle._config.host.length > 0);
            assert(fs.existsSync(destFile));
            var content = fs.readFileSync(destFile, {encoding:'utf-8'});

            assert(null === content.match(/\{\{[0-4]\}\}/));
            assert(null === content.match(/\{\{[\w-]+\}\}/));

            //         mg      bg-min    fg-min    bg-maj    fg-maj
            //.send('#00007f|4,#ffff11|3,#ffff22|2,#ffff33|1,#ffff44|0');

            // this array is sorted by (expected) sem asc
            const expectedColors = ['#ffff44','#ffff33','#ffff22','#ffff11','#00007f','#000000','#000000','#000000','#000000','#ffffff'];

            // this array is sorted by sem asc
            var colors = content.match(/#[0-9a-fA-f]{6}/g);

            assert(expectedColors.length === colors.length);
            for (var i = 0; i < expectedColors.length; i++) {
                assert.equal(expectedColors[i], colors[i]);
            }

            // clean-up
            fs.unlinkSync(destFile);
            handle.close();
            done();
        }, 500);
    });
});
