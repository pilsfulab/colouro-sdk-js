## 1.2.0 (2018-03-11)
Added support for black/white contrast template variable.

## 1.1.2 (2018-02-18)

Sync: fixed new var expansion to get indexed by semantic.

## 1.1.1 (2018-02-17)

Template variables has now more sensible names.

## 1.1.0 (2019-02-14)

Improved gulp-based workflow so no `npx colouro-sync` is needed if appropriate
changes are made into `gulpfile.js`.

## 1.0.1

Bug fix release

## 1.0.0 (2018-12-12)

Initial release
