import replace from 'rollup-plugin-replace';

export default {
    input: 'src/client.js',
    output: {
        file: 'lib/colouro-sdk.js',
        format: 'cjs',
        name: 'colouro'
    },
    plugins: [
        replace({
            'process.browser': false
        })
    ]
};
