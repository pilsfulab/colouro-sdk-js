# Colouro SDK

_[Colouro](https://colouroapp.com)_ is a mobile app for creating color schemes with unique instant preview feature. 
This package provides SDK with tools utilizing the color scheme instant preview.


# Installation

```
npm install -D colouro-sdk
```

# Overview

At the moment there are basically three ways you can use instant preview:

1. `colouro-reskin.js`: Client-side JavaScript library for quick CSS experiments.

    Pros:
    - Quick and easy way,
    - No dependencies, works in any modern browser, minimal configuration required.
    
    Cons:
    - The color scheme is not stored persistently.
    - Does not support CSS processors (SASS, LESS) and their color functions.

2. `colouro-sync` Developer server-side tool.

    Pros:
    - Persistent: the last version of color scheme is saved on developer computer.
    - Integrates well with CSS processors.
    
    Cons:
    - Requires a bit of configuration and SASS/LESS template to be created.
    - Solution is Node.js-based, which may not be your cup of tea.

3. Low-level access via `colouro.Client` client.

    Pros:
    - You can plug Colouro instant preview into your unique workflow not limited
      to web-design.
    
    Cons:
    - You have to write some code to handle received color scheme RGB values 
      and semantic.


# Colouro Reskin

`colouro-reskin.js` is browser-side library providing simple CSS reskin
feature. After installing `colouro-sdk` package the library is located 
in `<project>/node_modules/colouro-sdk/dist/colouro-reskin.js`

## Getting started

1. Copy `<project>/node_modules/colouro-sdk/dist/colouro-reskin.js` file
   to your script location.

2. Include the `colouro-reskin.js` script in your HTML file like this:
        
        ...
        <head>
           ...
            <script src="colouro-resking.js"></script>
        </head>
        ...

3.  Next define on-load function to be called when loading is finished.

        ...
        <head>
            ...
            <script>
            function onLoad() {
                
                colouro.reskin({
                    '#000000':0,  // original primary foreground color
                    '#ffffff':1,  // original primary background color
                    '#8bfe6f':2,  // original secondary foreground color
                    '#141f4e':3,  // original secondary background color
                    '#6b0e65':4   // original mid-ground color
                });
            }
            </script>
        </head>
        <body onload="onLoad();">
        ...
        
    Replace the color values in defined mappings with your colors as defined
    in stylesheet.

4. Start Colouro app and open the connection dialog.
5. Load the HTML file in browser. In top-right corner there should be small
   frame with _Colouro Host_ URL field. Enter the IP address as shown in connection 
   dialog (without the `http://` prefix) and click the _Connect_ button.
6. Once the Colouro client gets connected to the app, the frame will disappear, 
   and appears again if connection is lost.
   
## Complete example

Following HTML snippet is minimal page example:

```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Reskin Test</title>
    <style>
        html, body { margin: 0; }
        .major {
            font-size: 48pt;
            text-align: center;
            padding-top: 20vh;
            color: #000000;
            background-color: #ffffff;
            height: 30vh;
        }
        .minor {
            font-size: 48pt;
            text-align: center;
            color: #8bfe6f;
            background-color: #141f4e;
            height: 30vh;
            padding-top: 10vh;
        }
        .midground {
            height: 10vh;
            background-color: #6b0e65;
        }
    </style>
    <script type="text/javascript" src="js/colouro-reskin.js"></script>
    <script>
        function onLoad() {
            colouro.reskin({'#000000':0,'#ffffff':1, '#8bfe6f':2, '#141f4e':3, '#6b0e65':4});
        }
    </script>
</head>
<body onload="onLoad()">
    <div class="major">Lorem ipsum</div>
    <div class="midground">&nbsp;</div>
    <div class="minor">Lorem ipsum</div>
</body>
</html>

```

# Colouro Sync

_Colouro Sync_ is a tool for real-time synchronization of the colour scheme 
being edited in Colouro app. Each time color scheme is updated, it gets stored
into defined file.

## Getting started

1. Install `colouro-sdk` package into your node project:
    
    ```
    npm install --save-dev colouro-sdk
    ```
    
2. Create template CSS/SCSS/LESS file like this (Sass example named `scss/colors.scss.in`):
    
        $primary-foreground:     {{fg-maj}};
        $primary-background:     {{bg-maj}};
        $secondary-foreground:   {{fg-min}};
        $secondary-background:   {{bg-min}};
        $middleground:           {{mg}};
    
3. In root of your project create `colouro.json` file with this content:

        {
            "host": "192.168.0.42:8080",
            "template-file": "scss/colors.scss.in",
            "dest": "scss/colors.scss"
        }
    
4. In your SASS/LESS stylesheets:
    
    1. Include the file stated in `dest` field of `colouro.json` config,
    2. Use color variables.
    
5. Run Colouro app on your handset, connected to same wifi as the computer with node project.
    
    1. In the application open connect dialog and copy the address of your handset to 
    value of `host` field of `colouro.json` file.
    
6. Open terminal and run following npm command in root of your node project:

    ```
    npx colouro-sync
    ```

7. Run browser-sync task like `gulp dev`. Make sure that the generated file is watched for
changes.

    Tip: If you use gulp for building your project, you may omit the step 6 and run `colouro-sync` 
    within `gulp dev` process. See _Using with gulp_ section bellow for more details.


## Template file syntax

In _Getting started_ section we created simple Sass template without going into details. 
It may be easy to see that the template uses handlebar syntax with five named variables
being expanded. These variables denote individual color purpose (semantic)
as set in color scheme being edited in Colouro app.

- `{{fg-maj}}`---major foreground,
- `{{bg-maj}}`---major background,
- `{{fg-min}}`---minor foreground,
- `{{bg-min}}`---minor background,
- `{{mg}}`---mid-ground.
- `{{fg-maj-bw}}`, ..., `{{mg-bw}}`---black or white complementary color for given one 
(the one with higher contrast for better legibility. 

`colouro.json` config file
---
Colouro Sync uses simple json config file being located in project root under 
`colouro.json` file with following fields:

- `host` (string): IP address and port (always 8080) with Colouro app server running at handset, 
    e.g. `192.168.0.1:8080`
- `template-file` (string): relative file path of template file to be expanded with current scheme
    colors, e.g. `scss/colors.scss.in`,
- `dest` (string): relative file path of to be generated from template, e.g. `scss/colors.scss`,
- `verbose` (boolean): turn the verbose mode on/off.

## Version control

If your project uses some version control system like `git`, you may wonder what files
you should store in your repository so other team members can use Colouro real-time 
preview (e.g. during brainstorming session).

To share the Colouro workflow, you should store following files in repository:
- template file as stated in `template-file` field of `colouro.json` file,
- generated file as stated in `dest` field of `colouro.json` file,
- `colouro.json` file with configuration.

However storing `colouro.json` is recommend, there's stated a `host` with IP that may
change frequently. To simplify version control, you can create `colouro.local.json` file
in your project root with following content (replace the IP address with your handset 
address as displayed in Colouro app connect dialog):

```json
{
  "host":"10.20.30.40:8080"
}
``` 

The `colouro.local.json` file should marked as ignored for your version system. 
In case of `git` you may add the file into your `.gitignore` like this:

```
echo colouro.local.json >> .gitignore
```

## Using with gulp

There is a better way to setup your gulp-based workflow than running 
the `npx colouro-sync` command in own terminal window.

You may extend your `gulpfile.js` to make the `colouro-sync` 
functionality available right from gulp "files-watching-process".

There are just two simple steps:

1. At beginning of your `gulpfile.js` import a `colouroSync` function like this:

    ```
    const colouroSync = require('colouro-sdk').colouroSync;
    ```

2. Call the `colouroSync()` in any task that gets executed (usually at place 
   where `browserSync` is initialized). 
   
   For instance I'm used to workflow used by 
   [StartBootstrap templates](https://startbootstrap.com/templates/). In this case
   the "files-watching" task is called `dev`. This task depends on `browserSync` task
   which is responsible for intializing the [browserSync](https://www.browsersync.io/).
   That's the right place to initialize the colouroSync too like this:
   
   ```
   // Configure the browserSync task
   gulp.task('browserSync', function() {
   
     // NOTE: following line was added to initialize colouro-sync
     colouroSync();
   
     browserSync.init({
       server: {
         baseDir: "./"
       }
     });
   });
   ```
   
 Note that the example above still uses the `colouro.json` (and `colouro.local.json`)
 config files. All the config properties can also be passed to `colouroSync` function
 as object. See `colouroSync` JSDoc for more info.

# Low-level Client API

The central component of Colouro SDK is a Colouro client implementation that you
can utilize to plug the Colouro instant preview into you workflow.

## Getting started

Following snippet shows minimal example of Client class usage, that will print
status and received colors into console.

```js
// Import Colouro SDK
// (only for node.js, omit this line in browser, 
// instead include the colouro-reskin.js in your HTML)
var colouro = require('colouro-sdk'),

    // Semantic name constants 
    semanticNames = ['primary fg', 'primary bg', 'secondary fg', 'secondary bg', 'mg'],
    
    // Colouro app host address
    colouroHost = '192.168.0.42',
    
    // Create client instance with given host and default port
    client = new colouro.Client({host: colouroHost + ':8080'})
        
        // Setup status changed callback
        .on('status', function(connected) { 
            console.log(connected ? 'Connected' : 'Diconnected'); 
        })
        
        // Setup color scheme recevied callback
        .on('colors', function(colors, semantics) { 
            console.log('Colors received');
            for (var i = 0; i < 5; i++) {
                console.log(i + ': ' + colors[i] + ' (' + semanticNames[semantics[i]] + ')');
            }
        });

```

If you run this script with correct Colouro host, you may see something like this
(hit Ctrl-C to quit the script):

```
Connected
Colors received
0: #fcef7a (primary fg)
1: #284e4a (primary bg)
2: #b7fed4 (secondary bg)
3: #2b221e (secondary fg)
4: #ff2a1e (mg)
```

Notes:
1. As you can see the semantic (meaning of color) constants are in range 0..4, 
   see the `semanticNames` in example above.
2. If connecting to Colouro host fails, the client will try to reconnect forever.
   Each time the connection fails or succeeds the `status` callback gets called.
3. If you want to stop the client, call `client.close()`, to start client again
   call `client.init()`. 
   