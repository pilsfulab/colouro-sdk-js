
function regExpForString(searchStr) {
    // escape regexp special characters in search string
    searchStr = searchStr.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');

    return new RegExp(searchStr, 'gi');
}

function replaceAll(str, searchStr, replaceStr) {
    // escape regexp special characters in search string
    searchStr = searchStr.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');

    return str.replace(new RegExp(searchStr, 'gi'), replaceStr);
}

function CssPatcher(colorMappings)
{
    this.colorMappings = colorMappings;
    this.extendColorMappings();
    this.patchSet = {};
    this.exprs = [
        regExpForString('{{0}}'),
        regExpForString('{{1}}'),
        regExpForString('{{2}}'),
        regExpForString('{{3}}'),
        regExpForString('{{4}}')
    ];
    for (var i = 0; i < document.styleSheets.length; i++) {
        var css = document.styleSheets[i],
            cssPatches = {},
            addedAnyCssPatch = false;
        try {
            if (css.cssRules === undefined)
                continue;
        } catch (e) {
            continue;
        }
        for (var j = 0; j < css.cssRules.length; j++) {
            var rule = css.cssRules[j],
                rulePatches = {},
                addedAnyRulePatch = false;
            if (rule.type !== 1)
                continue;
            for (var k = 0; k < rule.style.length; k++) {
                var s = rule.style[k],
                    v = rule.style[s];
                if (v === undefined)
                    continue;
                var t = matchMappingToTemplate(v, this.colorMappings);
                if (t != null) {
                    rulePatches[s] = t;
                    addedAnyRulePatch = true;
                }
            }
            if (addedAnyRulePatch) {
                cssPatches[j] = rulePatches;
                addedAnyCssPatch = true;
            }
        }
        if (addedAnyCssPatch) {
            this.patchSet[i] = cssPatches;
        }
    }
}

CssPatcher.prototype.extendColorMappings = function () {
    var cm = {};
    for (var c in this.colorMappings) {
        if (!this.colorMappings.hasOwnProperty(c))
            continue;
        cm[c] = this.colorMappings[c];
        cm[hexToRgb(c)] = this.colorMappings[c];
    }
    this.colorMappings = cm;
};

CssPatcher.prototype.update = function (colors, semantics) {
    var cm = colorMapping(colors, semantics);
    for (var i in this.patchSet) {
        if (!this.patchSet.hasOwnProperty(i))
            continue;
        var css = document.styleSheets[i],
            cssPatch = this.patchSet[i];
        for (var j in cssPatch) {
            if (!cssPatch.hasOwnProperty(j))
                continue;
            var rule = css.cssRules[j],
                rulePatch = cssPatch[j];
            for (var k in rulePatch) {
                if (!rulePatch.hasOwnProperty(k))
                    continue;
                var t = rulePatch[k];
                rule.style[k] = this.expandTemplate(t, cm);
            }
        }
    }
};

CssPatcher.prototype.expandTemplate = function (t, colors) {
    var s = t;
    for (var i = 0; i < 5; i++) {
        if (!i in colors)
            continue;
        s = s.replace(this.exprs[i], colors[i]);
    }
    //console.log(t + '->' + s);
    return s;
};

function colorMapping(colors, semantics) {
    var ret = {};
    for (var i = 0; i < 5; i++) {
        ret[semantics[i]] = colors[i];
    }
    return ret;
}

function matchMappingToTemplate(rule, mappings) {
    var foundAny = false;
    for (var mapping in mappings) {
        if (!mappings.hasOwnProperty(mapping))
            continue;
        //console.log(rule);
        if (rule.indexOf(mapping) >= 0) {
            rule = replaceAll(rule, mapping, '{{' + mappings[mapping] + '}}');
            foundAny = true;
        }
    }
    return foundAny ? rule : null;
}

function hexToRgb(h) {
    var r = parseInt(h.substr(1,2), 16),
        g = parseInt(h.substr(3,2), 16),
        b = parseInt(h.substr(5,2), 16);
    return 'rgb(' + r + ', ' + g + ', ' + b + ')';
}

//module.exports = CssPatcher;
export default CssPatcher;