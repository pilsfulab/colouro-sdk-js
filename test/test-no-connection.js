var assert = require('assert'),
    Client = require('../lib/colouro-sdk'),
    server = null,
    client = null;
describe('Client without connection', function(){
    it('should receive disconnect event forever', function(done){
        var counter = 0;

        client = new Client({host:'192.168.0.113:8080', handshakeTimeout:50})
            .on('status', function (cnt){
                assert.equal(cnt, false);
                counter++;
                if (counter === 3) {
                    client.close();
                    done();
                }
            });
        this.timeout(10000);
    });
});