/**
 * @module colouro
 */

import Client from  './client.js';
import CssPatcher from './css-patcher.js';

export { Client, CssPatcher };

/**
 * Initialize Colouro Reskin module replacing colors specified by colorMapping connected to given host.
 * @param {Object} colorMapping
 * @param {string} host Hostname and port to Colouro Server running on handset within Colouro app.
 * E.g. 192.168.0.42:8080.
 * @example
 * colouro.reskin({
 *     // CSS Color to semantic mappings
 *     '#ffffff': 0, // map all white color CSS attributes to major foreground
 *     '#000000': 1, // map all black color CSS attributes to major background
 *     '#ff0000': 2, // map all red color CSS attributes to minor foreground
 *     '#ff00ff': 3, // map all purple color CSS attributes to minor background
 *     '#00ff00': 4  // map all green color CSS attributes to mid-ground.
 *   },
 *   '192.168.0.42:8080' // connect to Colouro app running at handset with IP address 192.168.0.42
 * );
 */
export function reskin(colorMapping, host) {
    var
        patcher = new CssPatcher(colorMapping),
        handler = function (colors, semantics) { patcher.update(colors, semantics); },
        client = null;

    if (host) {
        client = new Client({
            host: host,
            colorHandlerFunc: handler
        });
    } else {
        var elFrame = document.createElement('div'),
            elUrlInput = document.createElement('input'),
            elButton = document.createElement('button');
        elFrame.setAttribute('style',
            'position: absolute;' +
            'right: 0; top:0; white-space: nowrap;' +
            'background-color: #333; color: #fff;' +
            'text-align: center; ' +
            'border-radius: 10px;' +
            'padding: 10px;' +
            'box-shadow: 0 0 10px #000;');
        elUrlInput.value = '192.168.0.1:8080';
        elUrlInput.setAttribute('style', 'background-color: #ddd; color: #333; border: solid 1px #333;');
        elUrlInput.addEventListener('keyup', function (evt){
            evt.preventDefault();
            if (evt.key === 'Enter') {
                elButton.click();
                elButton.focus();
            }
        });
        elButton.appendChild(document.createTextNode('Connect'));
        elButton.setAttribute('style', 'background-color: #ddd; color: #333; border: solid 1px #333;');
        elButton.addEventListener('click', function(){
            if (client) {
                client.close();
                client = null;
            }

            var host = elUrlInput.value;
            if (host.indexOf(':') < 0) {
                host += ':8080';
            }
            client = new Client({host: host, colorHandlerFunc: handler})
                .on('status', function (status){
                    elFrame.style.display = status ? 'none':'block';
                });
        });

        elFrame.appendChild(document.createTextNode('Colouro Host: '));
        elFrame.appendChild(elUrlInput);
        elFrame.appendChild(elButton);
        document.body.appendChild(elFrame);

    }
}
