import replace from 'rollup-plugin-replace';
import uglify from 'rollup-plugin-uglify';

export default {
    input: 'src/css-reskin.js',
    output: {
        file: 'dist/colouro-reskin.js',
        format: 'iife',
        name: 'colouro'
    },
    plugins: [
        replace({
            'process.browser': true
        }),
        uglify.uglify()
    ]
};
