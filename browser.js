var cssReskin = (function (exports) {
    'use strict';

    if (!process.browser) {
        // import websocket API
        var WebSocket = require('ws');
        // imitate window api
        var window = {
            setTimeout: function (cb, timeout) {
                return setTimeout(cb, timeout);
            },
            location: {
                host: 'localhost:8080'
            }
        };
    }

    function Client(opts)
    {
        this.closing = false;
        this.verbose = opts.verbose || false;
        this.ws = null;
        this.timer = null;
        this.url = "ws://" + (opts.host || window.location.host) + "/colors";
        this.colorHandlerFunc = opts.colorHandlerFunc || function(){};
        this.events = {status:[], colors:[]};
        this.init();
    }

    Client.prototype.init = function() {
        this.ws = new WebSocket(this.url, null, {handshakeTimeout: 1000});
        var verbose = this.verbose;
        var recon = this.reconnect.bind(this);
        var msgRecv = this.onMessageReceived.bind(this);
        var invokeFn = this.invokeStatusChanged.bind(this);
        var self = this;
        this.ws.onopen = function(evnt) {
            invokeFn(true);
        };
        this.ws.onmessage = function(evnt) {
            var data = evnt.data;
            //this.onMessageReceived.bind(this, data);
            msgRecv(data);
        };
        this.ws.onerror = function (evnt) {
            if (verbose) {
                verbose(evnt);
            }
        };

        this.ws.onclose = function(evnt) {
            invokeFn(false);
            if (!self.closing) {
                window.setTimeout(recon, 1000);
            }
        };
    };

    Client.prototype.on = function(eventName, callback)
    {
        var cbs = this.events[eventName];
        if (cbs)
        {
            cbs.push(callback);
        }
        return this;
    };

    Client.prototype.invokeStatusChanged = function(status)
    {
        var i, cbs = this.events.status;
        for (i = 0; i < cbs.length; i++)
        {
            cbs[i](status);
        }
    };

    Client.prototype.invokeColorsChanged = function(colors, semantics)
    {
        var cbs = this.events.colors;
        for (var i = 0; i < cbs.length; i++)
        {
            cbs[i](colors, semantics);
        }
    };

    Client.prototype.onMessageReceived = function(data)
    {
        var colorData = data.split(",");
        var colors = [];
        var semantics = [];
        for (var i = 0; i < colorData.length; i++)
        {
            var tokens = colorData[i].split("|");
            colors.push(tokens[0]);
            semantics.push(parseInt(tokens[1]));
        }
        this.colorHandlerFunc(colors, semantics);
        this.invokeColorsChanged(colors, semantics);
    };

    Client.prototype.reconnect = function()
    {
        //this.ws.close();
        this.ws = null;
        try {
            this.init();
        } catch (e) {
            if (typeof this.verbose === 'function') {
                this.verbose(e);
            }
        }
    };

    Client.prototype.close = function ()
    {
        this.closing = true;
        this.ws.close();
    };

    function regExpForString(searchStr) {
        // escape regexp special characters in search string
        searchStr = searchStr.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');

        return new RegExp(searchStr, 'gi');
    }

    function replaceAll(str, searchStr, replaceStr) {
        // escape regexp special characters in search string
        searchStr = searchStr.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');

        return str.replace(new RegExp(searchStr, 'gi'), replaceStr);
    }

    function CssPatcher(colorMappings)
    {
        this.colorMappings = colorMappings;
        this.extendColorMappings();
        this.patchSet = {};
        this.exprs = [
            regExpForString('{{0}}'),
            regExpForString('{{1}}'),
            regExpForString('{{2}}'),
            regExpForString('{{3}}'),
            regExpForString('{{4}}')
        ];
        for (var i = 0; i < document.styleSheets.length; i++) {
            var css = document.styleSheets[i],
                cssPatches = {},
                addedAnyCssPatch = false;
            try {
                if (css.cssRules === undefined)
                    continue;
            } catch (e) {
                continue;
            }
            for (var j = 0; j < css.cssRules.length; j++) {
                var rule = css.cssRules[j],
                    rulePatches = {},
                    addedAnyRulePatch = false;
                if (rule.type !== 1)
                    continue;
                for (var k = 0; k < rule.style.length; k++) {
                    var s = rule.style[k],
                        v = rule.style[s];
                    if (v === undefined)
                        continue;
                    var t = matchMappingToTemplate(v, this.colorMappings);
                    if (t != null) {
                        rulePatches[s] = t;
                        addedAnyRulePatch = true;
                    }
                }
                if (addedAnyRulePatch) {
                    cssPatches[j] = rulePatches;
                    addedAnyCssPatch = true;
                }
            }
            if (addedAnyCssPatch) {
                this.patchSet[i] = cssPatches;
            }
        }
    }

    CssPatcher.prototype.extendColorMappings = function () {
        var cm = {};
        for (var c in this.colorMappings) {
            if (!this.colorMappings.hasOwnProperty(c))
                continue;
            cm[c] = this.colorMappings[c];
            cm[hexToRgb(c)] = this.colorMappings[c];
        }
        this.colorMappings = cm;
    };

    CssPatcher.prototype.update = function (colors, semantics) {
        var cm = colorMapping(colors, semantics);
        for (var i in this.patchSet) {
            if (!this.patchSet.hasOwnProperty(i))
                continue;
            var css = document.styleSheets[i],
                cssPatch = this.patchSet[i];
            for (var j in cssPatch) {
                if (!cssPatch.hasOwnProperty(j))
                    continue;
                var rule = css.cssRules[j],
                    rulePatch = cssPatch[j];
                for (var k in rulePatch) {
                    if (!rulePatch.hasOwnProperty(k))
                        continue;
                    var t = rulePatch[k];
                    rule.style[k] = this.expandTemplate(t, cm);
                }
            }
        }
    };

    CssPatcher.prototype.expandTemplate = function (t, colors) {
        var s = t;
        for (var i = 0; i < 5; i++) {
            if (!i in colors)
                continue;
            s = s.replace(this.exprs[i], colors[i]);
        }
        //console.log(t + '->' + s);
        return s;
    };

    function colorMapping(colors, semantics) {
        var ret = {};
        for (var i = 0; i < 5; i++) {
            ret[semantics[i]] = colors[i];
        }
        return ret;
    }

    function matchMappingToTemplate(rule, mappings) {
        var foundAny = false;
        for (var mapping in mappings) {
            if (!mappings.hasOwnProperty(mapping))
                continue;
            //console.log(rule);
            if (rule.indexOf(mapping) >= 0) {
                rule = replaceAll(rule, mapping, '{{' + mappings[mapping] + '}}');
                foundAny = true;
            }
        }
        return foundAny ? rule : null;
    }

    function hexToRgb(h) {
        var r = parseInt(h.substr(1,2), 16),
            g = parseInt(h.substr(3,2), 16),
            b = parseInt(h.substr(5,2), 16);
        return 'rgb(' + r + ', ' + g + ', ' + b + ')';
    }

    function colouroReskin(colorMapping, host) {
        var patcher = new CssPatcher(colorMapping),
            client = new Client({host: host, colorHandlerFunc: function(colors, semantics){
                patcher.update(colors, semantics);
            }});
    }

    exports.colouroReskin = colouroReskin;

    return exports;

}({}));
//# sourceMappingURL=browser.js.map
